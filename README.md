# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Importeert diensten van [KerkdienstGemist](https://www.kerkdienstgemist.nl) op basis van een RSS feed.

v0.1

### How do I get set up? ###
1. Download en installeer [Python](https://www.python.org/)
2. Download en installeer [`pip`](https://pip.pypa.io/en/stable/installing/) als dit nog niet gebeurd is.
3. Gebruik `pip` om `feedparser` te installeren. Dit kan in een command line met `pip install feedparser`.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
[Ben van der Stouwe](mailto:contact@benvanderstouwe.nl)